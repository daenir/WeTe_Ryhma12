list = [1,2,3, 5,6,7];
list2= [1,2,3,4,5,6,7,8,9];

describe("site module", function () {
    
    
    describe("ajax", function() {
        it("should make an AJAX request to the correct URL", function() {
            spyOn($, "ajax");
            getPicture("sloth.jpg");
            expect($.ajax.calls.mostRecent().args[0]["url"]).toEqual("Nettisivu/img/sloth.jpg");

        });
        
        it("should return image count", function() {
            spyOn($, "ajax").and.callFake(function(options) {
                options.success();
            });     
            var callback = jasmine.createSpy();
            getImgCount(callback);
            expect(callback).toHaveBeenCalled();
        });
        
        it("should return best image", function() {
            spyOn($, "ajax").and.callFake(function(options) {
                options.success();
            });     
            var callback = jasmine.createSpy();
            returnBestest(callback);
            expect(callback).toHaveBeenCalled();
        });

        it("should return newest image", function() {
            spyOn($, "ajax").and.callFake(function(options) {
                options.success();
            });     
            var callback = jasmine.createSpy();
            returnNewest(callback);
            expect(callback).toHaveBeenCalled();
        });
    });
    
    it("should return number not in list", function() {
        expect(randNum(list)).toEqual(4);
    });
    
    /*
    it("should return number not in list", function() {
        expect(randNum2(list2)).toEqual("list full");
    });
    */
});