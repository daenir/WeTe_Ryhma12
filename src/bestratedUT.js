
function getPicture(id) {
    $.ajax({
        type: "get",
        url: "Nettisivu/img/" + id,
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    });
}

function randNum(list) {
    var timer=0;
    do {
        randInt = Math.floor((Math.random() * 7) + 1); 
        console.log(timer);
        var found = $.inArray(randInt, list); //found = >0 jos luku on listassa
        if (found == -1) {
            list.push(randInt);
            return randInt;
        }
        timer++;
    } while (found > -1 || timer < 30);
    if (timer > 29) {
        return "list full";
    }
}

/*
function randNum2(list) {
    var timer=0;
    do {
        randInt = Math.floor((Math.random() * 7) + 1); 
        console.log(timer);
        var found = $.inArray(randInt, list); //found = >0 jos luku on listassa
        if (found == -1) {
            list.push(randInt);
            return randInt;
        }
        timer++;
    } while (found > -1 || timer < 30);
    if (timer > 29) {
        return "list full";
    }
}
*/
function returnBestest(callback) {
    $.ajax({
        url: '/Nettisivu/returnbest.php',
        type: 'get',
        success: callback
    })
}

function getImgCount(callback) {
    $.ajax({
        type: "get",
        url: "/Nettisivu/imgCount.php",
        success: callback
    })
}

function returnNewest(callback) {
    $.ajax({
        type: "get",
        url: "/Nettisivu/returnnewest.php",
        success: callback
    })
}

/*
function seuraava() {
    var imgcount;
    document.getElementById("ylänuoli").src = "assets/ylosmusta.gif";
    document.getElementById("alanuoli").src = "assets/alasmusta.gif";
    //PHP, joka laskee img kansion tiedostojen määrän
    $.ajax({
        url: 'imgCount.php',
        type: 'get',
        success: function(response) {
            imgcount = response; //imgcount=tiedostojen määrä img kansiossa
            if (list.length == imgcount) { //varoittaa jos kaikki kuvat selattu 
                alert("NO MORE PICS");
            } else {
                $.ajax({
                    url: 'returnbest.php',
                    type: 'get',
                    data: {
                        indeksi: indeksi
                    },
                    success: function(response) {
                        var data = $.parseJSON(response);
                        var foo = parseInt(data.id);
                        list.push(foo);
                        document.getElementById("kuva").src = data.nimi;
                        document.getElementById("pisteetmäärä").innerHTML = data.pisteet;
                        indeksi++;
                        console.log(list);
                    }
                });
                $(document.getElementById("ylös")).prop('disabled', false);
                $(document.getElementById("alas")).prop('disabled', false);
            };
        }
    });
};
    
*/