!DOCTYPE HTML>
<html>

<head>
  <title>Kuvalauta</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta charset="utf-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="jquery-3.1.1.js"></script>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index2.php">Kuva<span class="logo_colour">lauta</span></a></h1>
          <h2></h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="index2.php">Home</a></li>
          <li><a href="bestrated.php">Best rated pictures</a></li>
          <li><a href="newpics.php">New pics</a></li>
          <li class="selected"><a href="uploadpage.php">Upload new pic</a></li>
          <li><a href="contact.php">Contact Us</a></li>
        </ul>
      </div>
    </div>
    <div id="content_header"></div>
    <div id="site_content">
      
      <div id="content">
        <h1>Upload your own pictures</h1>
        <p>Here you can upload your own funny pictures to Kuvalauta. Just remember to keep it safe for work</p>
        <p>Remember that your pictures will be scaled to fit the site so they might not look the same as before submitting them. Only jpg, png and gif files are permitted.</p>
      
      <!-- UPLOAD LOMAKE, VAATII HIENOSÄÄTÖÄ-->
    <form action="upload.php" method="post" enctype="multipart/form-data">
    Select image to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload Image" name="submit">
    </form>
    </div>
    <div id="content_footer"></div>
    <div id="footer">
      <p><a href="index2.php">Home</a> | <a href="bestrated.php">Best rated pictures</a> | <a href="newpics.php">New Pictures</a> | <a href="uploadpage.php">Upload new pic</a> | <a href="contact.php">Contact Us</a></p>
    </div>
    <p>&nbsp;</p>
  </div>
</body>

</html>
