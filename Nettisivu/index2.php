<?php
// Start the session
session_start();
print_r($_SESSION["voted"]);
?>

!DOCTYPE HTML>
<html>

<head>
  <title>Kuvalauta</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta charset="utf-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="jquery-3.1.1.js"></script>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />

</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index2.php">Kuva<span class="logo_colour">lauta</span></a></h1>
          <h2></h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li class="selected"><a href="index2.php">Home</a></li>
          <li><a href="bestrated.php">Best rated pictures</a></li>
          <li><a href="newpics.php">New pics</a></li>
          <li><a href="uploadpage.php">Upload a new pic</a></li>
          <li><a href="contact.php">Contact Us</a></li>
        </ul>
      </div>
    </div>
    <div id="content_header"></div>
    <div id="site_content">
      
      <div id="content">
        <h1>Welcome to the Kuvalauta</h1>
        <p>Here are the funny pictures</p>
        
        
         <img class="img-responsive" id="kuva"/>
         <p></p>
          <div class="pisteet">
        <p id="pisteetviesti">Score: </p>
        <p id="pisteetmäärä"></p>
        </div>
         
             <div class="napit">
     <button type="button" name="previous" id="taakse" class="taakse" style="border: 0; background: transparent">
         <img src="assets/vasenmusta.gif" width="90" height="60"></button> 
     <button type="button" name="downvote" id="alas" class="alas" style="border: 0; background: transparent">
         <img src="assets/alasmusta.gif" width="90" height="60" id="alanuoli"></button> 
     <button type="button" name="upvote" id="ylös" class="ylös" style="border: 0; background: transparent">
         <img src="assets/ylosmusta.gif" width="90" height="60" id="ylänuoli"></button> 
     <button type="button" name="next" id="eteen" class="eteen" style="border: 0; background: transparent">
         <img src="assets/oikeamusta.gif" width="90" height="60"></button>
    </div>
    <div id="content_footer"></div>
    <div id="footer">
      <p><a href="index2.php">Home</a> | <a href="bestrated.php">Best rated pictures</a> | <a href="newpics.php">New pictures</a> | <a href="uploadpage.php">Upload a picture</a> | <a href="contact.php">Contact Us</a></p>
    </div>
    <p>&nbsp;</p>
  </div>
  <script src="/Nettisivu/js/index2.js"></script>
</body>

</html>
