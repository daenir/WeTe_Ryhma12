<?php

/* Redirect browser */
header("Location: https://projekti-simopar.c9users.io/Nettisivu/index2.php");
 
/* Make sure that code below does not get executed when we redirect. */

$host = "127.0.0.1";
$user = "simopar";                     
$pass = "";                               
$db = "mydb";                               
$port = 3306;                              
$con = mysqli_connect($host, $user, $pass);
mysqli_select_db($con, $db);

$target_dir = "img/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

$name = pathinfo($_FILES['fileToUpload']['name'], PATHINFO_FILENAME);
$extension = pathinfo($_FILES['fileToUpload']['name'], PATHINFO_EXTENSION);

$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
$increment = ''; //start with no suffix
if (file_exists($target_file)) {
    while(file_exists($target_dir . $name . $increment . '.' . $extension)) {
        $increment++;
    }
    $target_file = $target_dir . $name . $increment . '.' . $extension;
}

// Check file size
if ($_FILES["fileToUpload"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

//SQL lisäys
if ($check && $uploadOk) { //jos ladattava kuva on olemassa (ladattavan tiedoston koko>0)
/*
    
    $maxIDquery="SELECT MAX(ID) FROM Kuvat";
    if ($con->query($maxIDquery) === TRUE) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $con->error;
    }
    echo "maxID" . $maxID;
    echo "maxIDq" . $maxIDquery;
    $maxID = $con->query($maxIDquery);
    $maxID++;

    
    $query = "SELECT MAX(ID) FROM Kuvat";
    $result = mysqli_query($con, $query);
    
    $row=mysqli_fetch_array($result,MYSQLI_NUM);
    printf ($row[0]+1);
*/    
    $query = "INSERT INTO Kuvat (Nimi, Pisteet) VALUES ('$target_file', 0)";
    if ($con->query($query) === TRUE) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $con->error;
    }

    mysqli_close($con);
}

exit;
?>