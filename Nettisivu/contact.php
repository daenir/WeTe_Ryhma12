!DOCTYPE HTML>
<html>

<head>
  <title>Kuvalauta</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta charset="utf-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="jquery-3.1.1.js"></script>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index2.php">Kuva<span class="logo_colour">lauta</span></a></h1>
          <h2></h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="index2.php">Home</a></li>
          <li><a href="bestrated.php">Best rated pictures</a></li>
          <li><a href="newpics.php">New pics</a></li>
          <li><a href="uploadpage.php">Upload new pic</a></li>
          <li class="selected"><a href="contact.php">Contact Us</a></li>
        </ul>
      </div>
    </div>
    <div id="content_header"></div>
    <div id="site_content">
      
      <div id="content">
        <h1>Contact information</h1>
        <p>If you have any questions or inqueries please contact us with the following email</p>
        <p>Kuvalauta@funnypictures.com</p>
      
    </div>
    <div id="content_footer"></div>
    <div id="footer">
      <p><a href="index2.php">Home</a> | <a href="bestrated.php">Best rated pictures</a> | <a href="newpics.php">New Pictures</a> | <a href="uploadpage.php">Upload new pic</a> | <a href="contact.php">Contact Us</a></p>
    </div>
    <p>&nbsp;</p>
  </div>
</body>
</html>
