!DOCTYPE HTML>
<html>

<head>
  <title>Kuvalauta</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta charset="utf-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="jquery-3.1.1.js"></script>
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index2.php">Kuva<span class="logo_colour">lauta</span></a></h1>
          <h2></h2>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="index2.php">Home</a></li>
          <li><a href="bestrated.php">Best rated pictures</a></li>
          <li class="selected"><a href="newpics.php">New pics</a></li>
          <li><a href="uploadpage.php">Upload a new pic</a></li>
          <li><a href="contact.php">Contact Us</a></li>
        </ul>
      </div>
    </div>
    <div id="content_header"></div>
    <div id="site_content">
      
      <div id="content">
        <h1>New pictures</h1>
        <p>Here you can find the newest pictures uploaded to this site</p>
        
         <img class="img-responsive" id="kuva" src="" />
         <p></p>
          <div class="pisteet">
        <p id="pisteetviesti">Score: </p>
        <p id="pisteetmäärä"></p>
        </div>
         
             <div class="napit">
     <button type="button" name="previous" id="taakse" class="taakse" style="border: 0; background: transparent">
         <img src="assets/vasenmusta.gif" width="90" height="60"></button> 
     <button type="button" name="downvote" id="alas" class="alas" style="border: 0; background: transparent">
         <img src="assets/alasmusta.gif" width="90" height="60" id="alanuoli"></button> 
     <button type="button" name="upvote" id="ylös" class="ylös" style="border: 0; background: transparent">
         <img src="assets/ylosmusta.gif" width="90" height="60" id="ylänuoli"></button> 
     <button type="button" name="next" id="eteen" class="eteen" style="border: 0; background: transparent">
         <img src="assets/oikeamusta.gif" width="90" height="60"></button>
    </div>
    <div id="content_footer"></div>
    <div id="footer">
      <p><a href="index2.php">Home</a> | <a href="bestrated.php">Best rated pictures</a> | <a href="newpics.php">New pictures</a> | <a href="uploadpage.php">Upload a picture</a> | <a href="contact.php">Contact Us</a></p>
    </div>
    <p>&nbsp;</p>
  </div>
    <script src="/Nettisivu/js/newpics.js"></script>

</body>
<!--
<script type="text/javascript">
/* global $ */
window.onload = function() {
    var randInt;
    var list = []; //Lista mihin, näytettyjen kuvien ID tallennetaan
    var id;      //ID jolla kutsutaan kuvia (1,2,3...)
    var imgcount;
    //PHP, joka laskee img kansion tiedostojen määrän
    $.ajax({
        url: 'imgCount.php',
        type: 'get',
        success: function(response) {
            var imgcount=response;    //imgcount=tiedostojen määrä img kansiossa
            id=response;
            console.log(imgcount);
            if (list.length==imgcount) {           //varoittaa jos kaikki kuvat selattu 
                    alert("NO MORE PICS");
                }
            else {
                $.ajax({                        
                    url: 'returnnewest.php',
                    type: 'get',
                    data: {id: id},
                    success: function(response) {
                        var data = $.parseJSON(response);
                        list.push(parseInt(data.id));
                        document.getElementById("kuva").src=data.nimi;
                        document.getElementById("pisteetmäärä").innerHTML=data.pisteet;
                        id--;
                        console.log(list);  
                    }
                    });
                };
            }
        });

        //UPVOTE
    	var button1 = document.getElementById("ylös");
    	button1.onclick = function() {
    	    swapImage('assets/ylosvihreä.gif');
    		swapImage2('assets/alasmusta.gif');
    	    //pisteen lisäys tietokantaan
    	    $.ajax({
                url: 'vote.php',
                type: 'get',
                data: {id: list[list.length-1], suunta: 1},  //suunta=1 = upvote
                success: function(response) {
                    
                    //Kuvan uudelleenlataus tietokannasta
                    $.ajax({
                        url: 'Model.php',
                        type: 'get',
                        data: {id: id+1},
                        success: function(response) {
                            var data = $.parseJSON(response);
                            document.getElementById("kuva").src=data.nimi;
                            document.getElementById("pisteetmäärä").innerHTML=data.pisteet;
                        }
                    });
                }
            });
            $(this).prop('disabled', true);  //Disablee napin (voi vain painaa kerran)
            $(document.getElementById("alas")).prop('disabled', false)

        };
    	
        //DOWNVOTE
    	var button2 = document.getElementById("alas");
    	button2.onclick = function() {
    	    swapImage2('assets/alaspunainen.gif');
    		swapImage('assets/ylosmusta.gif');
            //pisteen poisto tietokannasta
            $.ajax({
                    url: 'vote.php',
                    type: 'get',
                    data: {id: list[list.length-1], suunta: 0},  //suunta=0 = downvote
                    success: function(response) {
                        //kuvan uudelleenlataus
                        $.ajax({
                            url: 'Model.php',
                            type: 'get',
                            data: {id: id+1},
                            success: function(response) {
                                var data = $.parseJSON(response);
                                document.getElementById("kuva").src=data.nimi;
                                document.getElementById("pisteetmäärä").innerHTML=data.pisteet;
                                }
                            });
                        }
                    });
                    $(this).prop('disabled', true);  //Disablee napin
                    $(document.getElementById("ylös")).prop('disabled', false)
    	};
        
        //UUSI KUVA
        var button3 = document.getElementById("eteen");
        button3.onclick = function() {
            document.getElementById("ylänuoli").src = "assets/ylosmusta.gif";
            document.getElementById("alanuoli").src = "assets/alasmusta.gif";
            var timer=0;
            var imgcount;
            
            //PHP, joka laskee img kansion tiedostojen määrän
            $.ajax({
                url: 'imgCount.php',
                type: 'get',
                success: function(response) {
                    imgcount=response;    //imgcount=tiedostojen määrä img kansiossa
                    if (id==0) {           //varoittaa jos kaikki kuvat selattu 
                    alert("NO MORE PICS");
                        }
                    else {
               
                    $.ajax({
                    url: 'returnnewest.php',
                    type: 'get',
                    data: {id: id},
                    success: function(response) {
                        var data = $.parseJSON(response);
                        list.push(parseInt(data.id));
                        document.getElementById("kuva").src=data.nimi;
                        document.getElementById("pisteetmäärä").innerHTML=data.pisteet;
                        id--;
                        console.log(data.nimi);
                        console.log(list);
                        
                    }
                    });
                    $(document.getElementById("ylös")).prop('disabled', false);
                    $(document.getElementById("alas")).prop('disabled', false);  //AKTIVOI
                    }
                }
            });
        };
        
        var button4 = document.getElementById("taakse");
        button4.onclick = function() {
            var lastItem = list[list.length-2];  //toiseksi viimeinen kuva
            console.log(lastItem);
            list.splice(-1,1);   //viimisen kuvan poisto
            console.log(list);
            if (lastItem==null) {
                alert("this is the first picture")
            }
            else {
            //NÄYTTÄÄ EDELLISEN KUVAN
            $.ajax({
            url: 'Model.php',
            type: 'get',
            data: {id: lastItem},
            success: function(response) {
                var data = $.parseJSON(response);
                document.getElementById("kuva").src=data.nimi;
                document.getElementById("pisteetmäärä").innerHTML=data.pisteet;
            }
            
            });
            }};
        
        
        var swapImage = function(src) {
            document.getElementById("ylänuoli").src = src;
        };

        var swapImage2 = function(src) {
            document.getElementById("alanuoli").src = src;
        };
};
   
</script>
-->
</html>
