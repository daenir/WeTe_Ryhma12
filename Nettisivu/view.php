<!DOCTYPE html>
<html>
    <!--
TODO
- VM316:1 Uncaught SyntaxError
- Uploadin jälkeen etusivulle redirect
- Kommentit ja niiden poistaminen
- Kuvan klikkaaminen avaa kuvan
- Pistejärjestys
- (IDJärjestys)
- 
-->
<head>
    <!--<script src="jquery-3.1.1.min.js"></script>
    -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="jquery-3.1.1.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/etusivutyyli.css" type="text/css" rel="stylesheet" />
    <title>Kuvalauta</title>

<script>

</script>
</head>  

<script type="text/javascript">

window.onload = function() {
    var randInt;
    var list = []; //Lista mihin, näytettyjen kuvien ID tallennetaan
    var timer=0;   //Varotoimenpide
            
            var imgcount;
            console.log(imgcount);
            //PHP, joka laskee img kansion tiedostojen määrän
            $.ajax({
                url: 'imgCount.php',
                type: 'get',
                success: function(response) {
                    var imgcount=response;    //imgcount=tiedostojen määrä img kansiossa
                    do {
                        randInt = Math.floor((Math.random() * imgcount) + 1);     //random luku välillä 1 - 3
                        console.log("rand" + randInt);
                        var found = $.inArray(randInt, list);              //found = >0 jos luku on listassa
                        timer++;                                           //found = -1 jos ei ole
                        if (found==-1) {
                            list.push(randInt);
                            break;
                        }
                    } while (found>-1 || timer < 25);
                    
                    $.ajax({                        
                    url: 'Model.php',
                    type: 'get',
                    data: {id: randInt},
                    success: function(response) {
                        var data = $.parseJSON(response);
                        document.getElementById("kuva").src=data.nimi;
                        document.getElementById("pisteetmäärä").innerHTML=data.pisteet;
                    }
                });
                    
                }
                });
        
        console.log(list);  
    	var div = document.getElementById("määrä");

        //UPVOTE
    	var button1 = document.getElementById("ylös");
    	button1.onclick = function() {
    	    swapImage('assets/ylösvihreä.gif');
    		swapImage2('assets/alasmusta.gif');
    	    //pisteen lisäys tietokantaan
    	    $.ajax({
                    url: 'vote.php',
                    type: 'get',
                    data: {id: list[list.length-1], suunta: 1},  //suunta=1 = upvote
                    success: function(response) {
                        
                        //Kuvan uudelleenlataus tietokannasta
                        $.ajax({
                            url: 'Model.php',
                            type: 'get',
                            data: {id: randInt},
                            success: function(response) {
                                var data = $.parseJSON(response);
                                document.getElementById("kuva").src=data.nimi;
                                document.getElementById("pisteetmäärä").innerHTML=data.pisteet;
                                }
                            });
                        }
                    });
                    $(this).prop('disabled', true);  //Disablee napin (voi vain painaa kerran)
                };
    	
        //DOWNVOTE
    	var button2 = document.getElementById("alas");
    	button2.onclick = function() {
    	    swapImage2('assets/alaspunainen.gif');
    		swapImage('assets/ylösmusta.gif');
            //pisteen poisto tietokannasta
            $.ajax({
                    url: 'vote.php',
                    type: 'get',
                    data: {id: list[list.length-1], suunta: 0},  //suunta=0 = downvote
                    success: function(response) {
                        //kuvan uudelleenlataus
                        $.ajax({
                            url: 'Model.php',
                            type: 'get',
                            data: {id: randInt},
                            success: function(response) {
                                var data = $.parseJSON(response);
                                document.getElementById("kuva").src=data.nimi;
                                document.getElementById("pisteetmäärä").innerHTML=data.pisteet;
                                }
                            });
                        }
                    });
                    $(this).prop('disabled', true);  //Disablee napin
    	};
        
        //UUSI KUVA
        var button3 = document.getElementById("eteen");
        button3.onclick = function() {
            var timer=0;
            var imgcount;
            
            //PHP, joka laskee img kansion tiedostojen määrän
            $.ajax({
                url: 'imgCount.php',
                type: 'get',
                success: function(response) {
                    imgcount=response;    //imgcount=tiedostojen määrä img kansiossa
                    do {
                        if (list.length==imgcount) {           //varoittaa jos kaikki kuvat selattu 
                            alert("NO MORE PICS");
                            break;
                        }
                        randInt = Math.floor((Math.random() * imgcount) + 1);     //random luku välillä 1 - imgkansion koko
                        var found = $.inArray(randInt, list);              //found = >0 jos luku on listassa
                        timer++;                                           //found = -1 jos ei ole
                        if (found==-1) {
                            list.push(randInt);
                            break;
                        }
                    } while (found>-1 || timer < 25);
            
                    console.log(list);
                    console.log(randInt);
                    $.ajax({
                        url: 'Model.php',
                        type: 'get',
                        data: {id: randInt},
                        success: function(response) {
                            console.log(response);
                            var data = $.parseJSON(response);
                            document.getElementById("kuva").src=data.nimi;
                            document.getElementById("pisteetmäärä").innerHTML=data.pisteet;
                        }
                    });
                    $(document.getElementById("ylös")).prop('disabled', false);
                }
            });
        };
        
        var button4 = document.getElementById("taakse");
        button4.onclick = function() {
            var lastItem = list[list.length-2];  //toiseksi viimeinen kuva
            console.log(lastItem);
            list.splice(-1,1);   //viimisen kuvan poisto
            console.log(list);
            
            //NÄYTTÄÄ EDELLISEN KUVAN
            $.ajax({
            url: 'Model.php',
            type: 'get',
            data: {id: lastItem},
            success: function(response) {
                var data = $.parseJSON(response);
                document.getElementById("kuva").src=data.nimi;
                document.getElementById("pisteetmäärä").innerHTML=data.pisteet;
            }
            });
        };

  };

var swapImage = function(src) {
    document.getElementById("ylänuoli").src = src;
};

var swapImage2 = function(src) {
    document.getElementById("alanuoli").src = src;
};
   
</script>

<body>

<!-- UPLOAD LOMAKE, VAATII HIENOSÄÄTÖÄ-->
<form action="upload.php" method="post" enctype="multipart/form-data">
    Select image to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload Image" name="submit">
</form>

    <img class="img-responsive" id="kuva" src="img/uguu.jpg" width="360" height="240"  alt=""/>
     
     <div id="zxc"></div>

    <div class="pisteet">
        <p id="pisteetviesti">Score: </p>
        <p id="pisteetmäärä"></p>
    </div>
    
       <div class="napit">
     <button type="button" name="previous" id="taakse" class="taakse" style="border: 0; background: transparent">
         <img src="assets/vasenmusta.gif" width="90" height="60"></button> 
     <button type="button" name="downvote" id="alas" class="alas" style="border: 0; background: transparent">
         <img src="assets/alasmusta.gif" width="90" height="60" id="alanuoli"></button> 
     <button type="button" name="upvote" id="ylös" class="ylös" style="border: 0; background: transparent">
         <img src="assets/ylösmusta.gif" width="90" height="60" id="ylänuoli"></button> 
     <button type="button" name="next" id="eteen" class="eteen" style="border: 0; background: transparent">

         <img src="assets/oikeamusta.gif" width="90" height="60"></button> 

</body>

</html>