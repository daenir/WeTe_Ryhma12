var list = [];  //lista johon, kuvien id talletetaan
var indeksi = 0;    //muuttuja joka kertoo monessako listan indeksissä liikutaan

function seuraava() {
    var imgcount;
    //Asettaa napit mustiksi
    swapImage('assets/ylosmusta.gif');
    swapImage2('assets/alasmusta.gif');
    //PHP, joka laskee img kansion tiedostojen määrän
    $.ajax({
        url: 'imgCount.php',
        type: 'get',
        success: function(response) {
            imgcount = response;            //imgcount=tiedostojen määrä img kansiossa
            if (list.length == imgcount) {  //varoittaa jos kaikki kuvat selattu 
                alert("NO MORE PICS");
            } else {
                $.ajax({
                    url: 'returnbest.php',
                    type: 'get',
                    data: {
                        indeksi: indeksi
                    },
                    success: function(response) {
                        var data = $.parseJSON(response);
                        var foo = parseInt(data.id);
                        list.push(foo);
                        document.getElementById("kuva").src = data.nimi;                    //Lataa kuvan näytölle
                        document.getElementById("pisteetmäärä").innerHTML = data.pisteet;   //Lataa pisteet näytölle
                        indeksi++;
                        console.log(list);
                    }
                });
                $(document.getElementById("ylös")).prop('disabled', false);     //Aktivoi napit
                $(document.getElementById("alas")).prop('disabled', false);
            };
        }
    });
};

function downvote() {
    //pisteen poisto tietokannasta
    $.ajax({
        url: 'vote.php',
        type: 'get',
        data: {
            id: list[list.length - 1],
            suunta: 0  //suunta=0 = downvote 
        }, 
        success: function(response) {
            //kuvan uudelleenlataus
            $.ajax({
                url: 'Model.php',
                type: 'get',
                data: {
                    id: list[indeksi - 1]
                },
                success: function(response) {
                    var data = $.parseJSON(response);
                    if (data.pisteet!=document.getElementById("pisteetmäärä").innerHTML) {
                        swapImage2('assets/alaspunainen.gif');      //jos piste on vaihdettu tietokantaan, punainen alasnuoli syttyy
                    }
                    document.getElementById("kuva").src = data.nimi;    //Lataa kuvan näytölle
                    document.getElementById("pisteetmäärä").innerHTML = data.pisteet;   //Lataa pisteet näytölle
                }
            });
        }
    });
};


function upvote() {
    //pisteen lisäys tietokantaan
    $.ajax({
        url: 'vote.php',
        type: 'get',
        data: {
            id: list[list.length - 1],
            suunta: 1  //suunta=1 = upvote
        }, 
        success: function(response) {
            //Kuvan uudelleenlataus tietokannasta
            $.ajax({
                url: 'Model.php',
                type: 'get',
                data: {
                    id: list[indeksi - 1]
                },
                success: function(response) {
                    console.log("indeksi " + indeksi);
                    var data = $.parseJSON(response);
                   
                    if (data.pisteet!=document.getElementById("pisteetmäärä").innerHTML) {
                        swapImage('assets/ylosvihreä.gif');      //jos piste on vaihdettu tietokantaan, vihreä ylösnuoli syttyy
                    }
                    document.getElementById("kuva").src = data.nimi;    //Lataa kuvan näytölle
                    document.getElementById("pisteetmäärä").innerHTML = data.pisteet;   //Lataa pisteet näytölle
                }
            });
        }
    });
};

function taaksepain() {
    swapImage('assets/ylosmusta.gif');
    swapImage2('assets/alasmusta.gif');
    var lastItem = list[list.length - 2]; //toiseksi viimeinen kuva
    console.log("lastItem" + lastItem);
    list.splice(-1, 1); //viimisen kuvan poisto
    console.log(list);
    indeksi--;

    //NÄYTTÄÄ EDELLISEN KUVAN
    $.ajax({
        url: 'Model.php',
        type: 'get',
        data: {
            id: lastItem
        },
        success: function(response) {
            if (lastItem == null) {     //varoittaa jos lista on tyhjä
                alert("first picture")
            } else {
                var data = $.parseJSON(response);
                document.getElementById("kuva").src = data.nimi;    //lataa kuvan näytölle
                document.getElementById("pisteetmäärä").innerHTML = data.pisteet;   //lataa pisteet näytölle
            }
        }
    });
};

//Vaihtaa ylänuolen
var swapImage = function(src) {
    document.getElementById("ylänuoli").src = src;
};

//Vaihtaa alanuolen
var swapImage2 = function(src) {
    document.getElementById("alanuoli").src = src;
};

function init() {
    seuraava();     //ensimmäisen kuvan lataus
    document.getElementById('eteen').onclick = function() {seuraava()};
    document.getElementById('ylös').onclick = function() {upvote()};
    document.getElementById('alas').onclick = function() {downvote()};
    document.getElementById('taakse').onclick = function() {taaksepain()};
}

window.onload = init;