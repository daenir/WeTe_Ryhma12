var randInt;
var list = []; //Lista mihin, näytettyjen kuvien ID tallennetaan
var timer = 0; //Varotoimenpide

var imgcount;
console.log(imgcount);
//PHP, joka laskee img kansion tiedostojen määrän
function seuraava() {
    var imgcount;
    //asettaa napit mustiksi
    swapImage('assets/ylosmusta.gif');
    swapImage2('assets/alasmusta.gif');
    $.ajax({
        url: 'imgCount.php',
        type: 'get',
        success: function(response) {
            var imgcount = response; //imgcount=tiedostojen määrä img kansiossa
            if (list.length == imgcount) {  //varoittaa jos kaikki kuvat selattu 
                alert("NO MORE PICS");
            } else {
            do {
                randInt = Math.floor((Math.random() * imgcount) + 1); //random luku välillä 1 - kaikki
                console.log("rand" + randInt);
                var found = $.inArray(randInt, list);   //found = >0 jos luku on listassa
                timer++;                                //found = -1 jos ei ole
                if (found == -1) {
                    list.push(randInt);
                    break;
                }
            } while (found > -1 || timer < 25);

            $.ajax({
                url: 'Model.php',
                type: 'get',
                data: {
                    id: randInt
                },
                success: function(response) {
                    var data = $.parseJSON(response);
                    document.getElementById("kuva").src = data.nimi;    //Lataa kuvan näytölle
                    document.getElementById("pisteetmäärä").innerHTML = data.pisteet;   //Lataa pisteet näytölle
                    console.log(list);
                }
            });
            $(document.getElementById("ylös")).prop('disabled', false);
            $(document.getElementById("alas")).prop('disabled', false);
        }}
    });
}

function upvote() {
    //pisteen lisäys tietokantaan
    $.ajax({
        url: 'vote.php',
        type: 'get',
        data: {
            id: list[list.length - 1],
            suunta: 1   //suunta=1 = upvote
        }, 
        success: function(response) {
            //Kuvan uudelleenlataus tietokannasta
            $.ajax({
                url: 'Model.php',
                type: 'get',
                data: {
                    id: list[list.length-1]
                },
                success: function(response) {
                    var data = $.parseJSON(response);
                    if (data.pisteet!=document.getElementById("pisteetmäärä").innerHTML) {
                        swapImage('assets/ylosvihreä.gif');     //jos piste on vaihdettu tietokantaan, vihreä ylösnuoli syttyy
                    }
                    document.getElementById("kuva").src = data.nimi;    //Lataa kuvan näytölle
                    document.getElementById("pisteetmäärä").innerHTML = data.pisteet;   //Lataa pisteet näytölle
                }
            });
        }
    });
};

function downvote() {
    //pisteen poisto tietokannasta
    $.ajax({
        url: 'vote.php',
        type: 'get',
        data: {
            id: list[list.length - 1],
            suunta: 0  //suunta=0 = downvote
        }, 
        success: function(response) {
            //kuvan uudelleenlataus
            $.ajax({
                url: 'Model.php',
                type: 'get',
                data: {
                    id: list[list.length-1]
                },
                success: function(response) {
                    var data = $.parseJSON(response);
                    if (data.pisteet!=document.getElementById("pisteetmäärä").innerHTML) {
                        swapImage2('assets/alaspunainen.gif');  //jos piste on vaihdettu tietokantaan, punainen alasnuoli syttyy
                    }
                    document.getElementById("kuva").src = data.nimi;     //Lataa kuvan näytölle
                    document.getElementById("pisteetmäärä").innerHTML = data.pisteet;   //Lataa pisteet näytölle
                }
            });
        }
    });
    //$(document.getElementById("alas")).prop('disabled', true); //Disablee napin
    //$(document.getElementById("ylös")).prop('disabled', false);
};
  
function taaksepain() {
   swapImage('assets/ylosmusta.gif');
    swapImage2('assets/alasmusta.gif');
    var lastItem = list[list.length - 2]; //toiseksi viimeinen kuva
    console.log(lastItem);
    list.splice(-1, 1); //viimisen kuvan poisto
    console.log(list);
    //NÄYTTÄÄ EDELLISEN KUVAN
    $.ajax({
        url: 'Model.php',
        type: 'get',
        data: {
            id: lastItem
        },
        success: function(response) {
            console.log(lastItem);
            if (lastItem == null) {     //varoittaa jos lista on tyhjä
                alert("first picture")
            } else {
                var data = $.parseJSON(response);
                document.getElementById("kuva").src = data.nimi;
                document.getElementById("pisteetmäärä").innerHTML = data.pisteet;
            }
        }
    });
};

//Vaihtaa ylänuolen
var swapImage = function(src) {
    document.getElementById("ylänuoli").src = src;
};

//Vaihtaa alanuolen
var swapImage2 = function(src) {
    document.getElementById("alanuoli").src = src;
};

function init() {
    'use strict';
    seuraava();     //ensimmäisen kuvan lataus
    document.getElementById('eteen').onclick = function() {
        seuraava()
    };
    document.getElementById('ylös').onclick = function() {
        upvote()
    };
    document.getElementById('alas').onclick = function() {
        downvote()
    };
    document.getElementById('taakse').onclick = function() {
        taaksepain()
    };
}

window.onload = init;